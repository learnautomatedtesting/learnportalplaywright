## LEARN AUTOMATED TESTING PORTAL


## Key Steps
- Automate transactions that can be performed on the portal.
- Navigate between articles and verify the functionality of buttons on the page using filters.

## Requirements
- Visual Studio Code
- Playwright
- Node.js
- [Learn Automated Testing website](https://staging.learnautomatedtesting.com/)

## Project Structure
- **tests/**: Store your Playwright test scenarios in this directory.
- **tsconfig.json**: TypeScript build settings.
- **package.json**: Project dependencies and script configurations.

## Setup on Your Operating System
Create a project and install node.js and Visual Studio Code according to the instructions.

## Setting Up the Test Framework
Install Playwright in Visual Studio Code through the terminal. Follow the instructions and choose TypeScript as the language.

## Page Objects
In this directory, web pages receive a separate file where all elements are stored and functions are described. These functions are called in the test script. We try to keep the elements in these functions as variable as possible. This ensures that the test script remains clear and that different elements can be used without having to adjust the test script.

## Specifications
Test scripts are defined in spec files.

## Commands
The following commands were used in executing the test script:
- `.waitForTimeout()`
- `.click()`
These can be found on the Playwright website.

## Running Tests
You can use the following command to execute the Playwright test scenarios included in the project:
```bash
npx playwright test