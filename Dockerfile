# Use the official Playwright image as the base - Version 1.17.1
FROM mcr.microsoft.com/playwright:v1.17.1-focal

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json (if available)
COPY package*.json ./

# Install project dependencies
RUN npm install

RUN npx playwright install  

# Copy the rest of the application code to the working directory
COPY . .

# Ensure all browsers needed by Playwright are installed
# This step might not be necessary as the Playwright Docker image comes with browsers pre-installed.
# However, if you've updated Playwright or need additional browsers, uncomment and adjust the next line:
# RUN npx playwright install

# Use the Playwright test runner to run tests
# Note: This CMD will run `npx playwright test` when the container starts.
CMD ["npx", "playwright", "test"]
