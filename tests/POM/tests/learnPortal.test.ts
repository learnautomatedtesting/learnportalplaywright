import { test, expect } from '@playwright/test';
import { learnPortal } from '../pages/learnPortal';
import * as data from '../testData/data.json';

test.describe('learnPortal testen', () => {
    // Using test.beforeEach for setup steps that apply to all tests within this describe block
    test.beforeEach(async ({ page }) => {
        const portaal = new learnPortal(page);
        await portaal.GaNaarPortal(data.url);
        await page.waitForTimeout(2000);
    });

    test('Ga naar de website', async ({ page }) => {
        const portaal = new learnPortal(page);    
        await portaal.verifieerPagina(data.url);
    });

    test('Ga naar de Blog', async ({ page }) => {
        const portaal = new learnPortal(page);
        await portaal.clickBlog();    
        await portaal.verifieerBlog(); // Verifieren we de blog pagina
    });

   
     test('Programming Language filter testen in de Blog', async({page}) => {
        const portaal = new learnPortal(page);
         await portaal.clickBlog();    
        await portaal.prorammingLang(2); // 1 HTML -- 2 JavaScript

         console.log("Huidige URL adres : "+ page.url());

        portaal.TitleNietHetzelfde(data.JavaScriptTitle);
    });
});
