import { Page, expect } from "@playwright/test";


export class learnPortal {
    page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async GaNaarPortal(url: string) {
        await this.page.goto(url);
        await this.page.waitForTimeout(5000);
    }

    async verifieerPagina(url: string) {
        expect(url).toBe(this.page.url());
    }

    // blog = () => this.page.locator("a[href='/blog/']");
    async clickBlog() {
        await this.page.click("a[href='/blog/']");
    }

    async verifieerBlog() {
        const TekstHugo = this.page.locator("[class='md:min-h-[130px]']").nth(0);
        await expect(TekstHugo).toBeVisible();
    }

    async clickToolsButton() {
        await this.page.getByRole('heading', { name: 'TOOLS', exact: true }).click();
        await this.page.getByLabel('Demoportal').check();
    }

    // async clickSrtByBtn(sorterenType: String) { // sorterenType oldest - Latest
    //     await this.page.click("//*[text()='SORT BY']");
    //     await this.page.locator("[class='text-base text-primary py-1 w-full px-3 hover:bg-slate-50']", {
    //         hasText: sorterenType,
    //     }).click();
    // }

    async clickSortByBtn(sorterenType: string): Promise<void> {
        try {
            // Click the "SORT BY" button, waiting for it to be clickable.
            await this.page.click("//*[text()='SORT BY']");
    
            // Define a more specific selector for the sorting option if possible.
            // This example assumes 'sorterenType' directly matches the text content of the sorting option.
            const sortingOptionSelector = `.text-base.text-primary.py-1.w-full.px-3.hover\\:bg-slate-50 >> text="${sorterenType}"`;
    
            // Wait for the sorting option to be visible and clickable.
            await this.page.waitForSelector(sortingOptionSelector, { state: 'visible' });
    
            // Click the sorting option.
            await this.page.click(sortingOptionSelector);
        } catch (error) {
            console.error(`Failed to click sort by button and select '${sorterenType}':`, error);
            // Re-throw the error or handle it as necessary for your test flow.
            throw error;
        }
    }

    async prorammingLang(index: number) {
        await this.page.click("//h3[text()='programming language']");
        switch (index) {
            case 1: "HTML";
                await this.page.locator("[class='text-base text-primary py-1 w-full px-3 hover:bg-slate-50']", {
                    hasText: "HTML",
                }).click();
                break;
            case 2: "JavaScript";
                await this.page.locator("[class='text-base text-primary py-1 w-full px-3 hover:bg-slate-50']", {
                    hasText: "JavaScript",
                }).click();
                break;
            default: "JavaScript";
                await this.page.locator("[class='text-base text-primary py-1 w-full px-3 hover:bg-slate-50']", {
                    hasText: "JavaScript",
                }).click();
                break;
        }
    }
    
    async TitleNietHetzelfde(TitleTekst: String) {
        expect(this.page.title()).not.toEqual(TitleTekst);
    }









}